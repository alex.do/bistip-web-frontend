angular.module('bistipWeb.user')
.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
  'use strict';
  $stateProvider
    .state('registration', {
      url: '/registration',
      views: {
        'content': {
          templateUrl: '/components/user/registration.html',
        },
        'header': {
          templateUrl: '/shared/header/header.html'
        },
        'footer': {
          templateUrl: '/shared/footer/footer.html'
        }
      }
    })
    .state('login', {
      url: '/login',
      views: {
        'content': {
          templateUrl: '/components/user/login.html',
        },
        'header': {
          templateUrl: '/shared/header/header.html'
        },
        'footer': {
          templateUrl: '/shared/footer/footer.html'
        }
      }
    });
}]);
