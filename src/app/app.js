
angular.module('bistipWeb', [
  'ui.router',
  'bistipWeb.user'
])
.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
  'use strict';
  $urlRouterProvider.otherwise('/dashboard');
  $stateProvider
    .state('dashboard', {
      url: '/dashboard',
      views: {
        'content': {
          templateUrl: '/components/dashboard/dashboard.html',
        },
        'header': {
          templateUrl: '/shared/header/header.html'
        },
        'footer': {
          templateUrl: '/shared/footer/footer.html'
        }
      }
    });
}]);
